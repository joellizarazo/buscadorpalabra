/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author JOEL
 */
public class MetodoBurbuja {
    //Ordenamiento por Burbuja
    private String palabras[];

    public MetodoBurbuja() {
    }

    public MetodoBurbuja(Object lineas[], int cantidad)throws Exception  {
        if(cantidad > lineas.length){
            throw new Exception("valores de indices fuera de rango");
        }
        this.palabras=new String[cantidad];
        for(int i = 0; i < cantidad ;i++){ //Tiempo: lineas.length          
            String cadena = lineas[i].toString();
            for(int j=0; j<cadena.length();j++){//Tiempo: cadena.length
                this.palabras[i]=cadena.toString();
            }
        }
    }
    
    //METODO BURBUJA
    public void burbuja(){
        Tiempo_Ejecucion t1 = new Tiempo_Ejecucion();
        String aux;
        for(int i=1; i<this.palabras.length;i++){
            for(int j=this.palabras.length-1; j >= i; j--){
                if(this.palabras[j].compareToIgnoreCase(this.palabras[j-1]) > 0){
                    aux = this.palabras[j];
                    this.palabras[j] = this.palabras[j-1];
                    this.palabras[j-1] = aux;
                }
            }
        }
        t1.stop();
        System.err.println("Tiempo de ordenamiento por burbuja es: " + t1.getTiempoMillis()+ "mseg");
    }
    
    //METODO BURBUJA MEJORADA
    public void burbujaMejorada(){
        Tiempo_Ejecucion t1 = new Tiempo_Ejecucion();
        String aux;
        int bandera=1;
        for(int i=0;i<this.palabras.length-1&&bandera==1;i++){
            bandera=0;
            for(int j=0;j<this.palabras.length-i-1;j++){
                if(this.palabras[j].compareToIgnoreCase(this.palabras[j+1])<0){
                    bandera=1;
                    aux=this.palabras[j];
                    this.palabras[j]=this.palabras[j+1];
                    this.palabras[j+1]=aux;
                }
            }
        }
        t1.stop();
        System.err.println("Tiempo de ordenamiento por burbuja mejorada es: " + t1.getTiempoMillis()+ "mseg");
    }
    
    @Override
     public String toString(){
        String msg = "";
        for(int i=0;i<this.palabras.length;i++){ 
                msg+=this.palabras[i]+"\t";
            msg+="\n";
        }
        return msg;
    }
}
