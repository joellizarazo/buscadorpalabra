/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

/**
 *
 * @author JOEL
 */ 
import Negocio.MetodoBurbuja;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author JOEL
 */
public class Prueba {
    public void palabra_1000(){
        try{
        ArchivoLeerURL archivo = new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt"); 
            Object lineas[]= archivo.leerArchivo();
            int canLineas=1000;
            System.out.println("-----------------1000-------------------");
            MetodoBurbuja m = new MetodoBurbuja(lineas,canLineas);
            m.burbuja();
            ArchivoLeerURL archivo2 = new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt"); 
            Object lineas2[]= archivo2.leerArchivo();
            MetodoBurbuja n = new MetodoBurbuja(lineas,canLineas);
            n.burbujaMejorada();
        } catch (Exception ex){
            System.err.println(ex.getMessage());
        }
    }
    
    public void palabra_5000(){
        try{
        ArchivoLeerURL archivo = new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt"); 
            Object lineas[]= archivo.leerArchivo();
            int canLineas=5000;
            System.out.println("------------------5000------------------");
            MetodoBurbuja m = new MetodoBurbuja(lineas,canLineas);
            m.burbuja();
            ArchivoLeerURL archivo2 = new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt"); 
            Object lineas2[]= archivo2.leerArchivo();
            MetodoBurbuja n = new MetodoBurbuja(lineas,canLineas);
            n.burbujaMejorada();
        } catch (Exception ex){
            System.err.println(ex.getMessage());
        }
    }
    
    public void palabra_10000(){
        try{
        ArchivoLeerURL archivo = new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt"); 
            Object lineas[]= archivo.leerArchivo();
            int canLineas=10000;
            System.out.println("------------------10000------------------");
            MetodoBurbuja m = new MetodoBurbuja(lineas,canLineas);
            m.burbuja();
            ArchivoLeerURL archivo2 = new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt"); 
            Object lineas2[]= archivo2.leerArchivo();
            MetodoBurbuja n = new MetodoBurbuja(lineas,canLineas);
            n.burbujaMejorada();
        } catch (Exception ex){
            System.err.println(ex.getMessage());
        }
    }
    
    public void palabra_20000(){
        try{
        ArchivoLeerURL archivo = new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt"); 
            Object lineas[]= archivo.leerArchivo();
            int canLineas=20000;
            System.out.println("------------------20000------------------");
            MetodoBurbuja m = new MetodoBurbuja(lineas,canLineas);
            m.burbuja();
            ArchivoLeerURL archivo2 = new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt"); 
            Object lineas2[]= archivo2.leerArchivo();
            MetodoBurbuja n = new MetodoBurbuja(lineas,canLineas);
            n.burbujaMejorada();
        } catch (Exception ex){
            System.err.println(ex.getMessage());
        }
    }
    
    public void palabra_30000(){
        try{
        ArchivoLeerURL archivo = new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt"); 
            Object lineas[]= archivo.leerArchivo();
            int canLineas=30000;
            System.out.println("------------------30000------------------");
            MetodoBurbuja m = new MetodoBurbuja(lineas,canLineas);
            m.burbuja();
            ArchivoLeerURL archivo2 = new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt"); 
            Object lineas2[]= archivo2.leerArchivo();
            MetodoBurbuja n = new MetodoBurbuja(lineas,canLineas);
            n.burbujaMejorada();
        } catch (Exception ex){
            System.err.println(ex.getMessage());
        }
    }
    
    public void palabra_80000(){
        try{
        ArchivoLeerURL archivo = new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt"); 
            Object lineas[]= archivo.leerArchivo();
            int canLineas=80000;
            System.out.println("------------------80000------------------");
            MetodoBurbuja m = new MetodoBurbuja(lineas,canLineas);
            m.burbuja();
            ArchivoLeerURL archivo2 = new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt"); 
            Object lineas2[]= archivo2.leerArchivo();
            MetodoBurbuja n = new MetodoBurbuja(lineas,canLineas);
            n.burbujaMejorada();
        } catch (Exception ex){
            System.err.println(ex.getMessage());
        }
    }
    
    public static void main(String[] args){
            //System.out.println(m.toString());
        Prueba n = new Prueba();    
        n.palabra_1000();
        n.palabra_5000();
        n.palabra_10000();
        n.palabra_20000();
        n.palabra_30000();
        n.palabra_80000();
    }
}
